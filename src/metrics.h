// Copyright (c) 2016 The Zcash developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or https://www.opensource.org/licenses/mit-license.php .

#ifndef ZCASH_METRICS_H
#define ZCASH_METRICS_H

#include "uint256.h"
#include "consensus/params.h"

#include <atomic>
#include <mutex>
#include <optional>
#include <string>

struct AtomicCounter {
    std::atomic<uint64_t> value;

    AtomicCounter() : value {0} { }

    void increment(){
        ++value;
    }

    void decrement(){
        --value;
    }

    int get() const {
        return value.load();
    }
};

class AtomicTimer {
private:
    std::mutex mtx;
    uint64_t threads;
    int64_t start_time;
    int64_t total_time;

public:
    AtomicTimer() : threads(0), start_time(0), total_time(0) {}

    /**
     * Starts timing on first call, and counts the number of calls.
     */
    void start();

    /**
     * Counts number of calls, and stops timing after it has been called as
     * many times as start().
     */
    void stop();

    bool running();

    uint64_t threadCount();

    double rate(const AtomicCounter& count);
};

enum DurationFormat {
    FULL,
    REDUCED
};

extern AtomicCounter transactionsValidated;
extern AtomicCounter ehSolverRuns;
extern AtomicCounter solutionTargetChecks;
extern AtomicTimer miningTimer;
extern std::atomic<size_t> nSizeReindexed; // valid only during reindex
extern std::atomic<size_t> nFullSizeToReindex; // valid only during reindex

void TrackMinedBlock(uint256 hash);

void MarkStartTime();
double GetLocalSolPS();
int EstimateNetHeight(const Consensus::Params& params, int currentBlockHeight, int64_t currentBlockTime);
std::optional<int64_t> SecondsLeftToNextEpoch(const Consensus::Params& params, int currentHeight);
std::string DisplayDuration(int64_t time, DurationFormat format);
std::string DisplaySize(size_t value);
std::string DisplayHashRate(double value);

void TriggerRefresh();

void ConnectMetricsScreen();
void ThreadShowMetricsScreen();

/**
 * Rendering options:
 * Agape: img2txt -W 40 -H 20 -f utf8 -d none -g 0.7 agape.png
 */
const std::string METRICS_ART =                                      
"            [0;30;5;40;100m8S[0;36;5;40;100m;..........:[0;30;5;40;100m%8[0m            \n"
"         [0;30;5;40;100mX[0;36;5;40;100m:......::;;;:.......%[0m      [0;1;37;97;47m.[0;1;30;90;47m [0m \n"
"       [0;36;5;40;100m;....:%[0;34;5;40;100mX8[0;1;30;90;44m888888888[0;34;5;40;100m@S[0;36;5;40;100m;....: [0;1;30;90;47mX[0;1;37;97;47m8[0;37;5;47;107m.[0m   \n"
"     [0;36;5;40;100mt....t[0;34;5;40;100m@[0;1;30;90;44m88888888888888888[0;34;5;40;100mS[0;36;5;40;100m [0;1;30;90;47m;[0;1;37;97;47m:[0;1;30;90;47m%:[0;37;5;47;107m%[0m    \n"
"    [0;36;5;40;100m:...;[0;34;5;40;100m8[0;1;30;90;44m888888888888888888[0;37;5;40;100mS[0;1;30;90;47m:X[0;36;5;40;100m: [0;1;37;97;47m8[0;37;5;47;107m@[0;36;5;40;100m [0;30;5;40;100m@[0m   \n"
"   [0;36;5;40;100m:...%[0;1;30;90;44m88888888888888888[0;36;5;40;100m@[0;1;30;90;47mS;[0;36;5;40;100m:[0;1;30;90;44m88[0;37;5;40;100mS[0;37;5;47;107mt[0;1;37;97;47mS[0;36;5;40;100m ..[0;30;5;40;100m8[0m  \n"
"  [0;36;5;40;100m:...t[0;1;30;90;44m888[0;34;5;40;100m8[0;1;30;90;47m@[0;1;37;97;47m%:[0;1;30;90;47mX88@X%8[0;1;30;90;44m@8[0;36;5;40;100m [0;1;37;97;47m [0;1;30;90;47m@[0;34;5;40;100m8[0;1;30;90;44m88@[0;1;37;97;47m.[0;37;5;47;107m.[0;1;30;90;47m@[0;34;5;40;100mX[0;36;5;40;100m....[0m  \n"
" [0;1;30;90;40m8[0;36;5;40;100m....[0;34;5;40;100m8[0;1;30;90;44m88[0;36;5;40;100m.[0;37;5;47;107mt8[0;36;5;40;100mS[0;1;30;90;44m8888888[0;36;5;40;100m:[0;37;5;47;107m@8[0;37;5;40;100mS[0;1;30;90;44m8888[0;36;5;40;100mt[0;37;5;47;107m@t[0;36;5;40;100m [0;1;30;90;44m88[0;36;5;40;100mt...[0;30;5;40;100m%[0m \n"
" [0;30;5;40;100mX[0;36;5;40;100m...:[0;1;30;90;44m88[0;36;5;40;100m [0;37;5;47;107m.[0;1;37;97;47m%[0;1;30;90;44m88888888[0;1;30;90;47m8[0;1;37;97;47m [0;36;5;40;100m;[0;1;37;97;47mt[0;1;30;90;47mX[0;1;30;90;44m888[0;1;30;90;47mS[0;37;5;47;107m.8[0;36;5;40;100m@[0;1;30;90;44m888[0;34;5;40;100mS[0;36;5;40;100m....[0m \n"
" [0;30;5;40;100mX[0;36;5;40;100m...:[0;1;30;90;44m88[0;37;5;47;107m%t[0;1;30;90;44m@888888[0;1;30;90;47m8[0;1;37;97;47m:[0;36;5;40;100m;[0;1;30;90;44m88[0;36;5;40;100m8[0;37;5;47;107mt[0;1;30;90;44m8[0;36;5;40;100m@[0;1;37;97;47m8[0;37;5;47;107m [0;1;37;97;47mt[0;1;30;90;44m@8888[0;34;5;40;100mS[0;36;5;40;100m....[0m \n"
" [0;30;5;40;100m8[0;36;5;40;100m....[0;34;5;40;100m8[0;1;30;90;44m8[0;37;5;47;107m:8[0;1;30;90;44m88888[0;36;5;40;100m%[0;1;37;97;47mt[0;37;5;40;100mX[0;1;30;90;44m88888[0;37;5;47;107m.[0;1;30;90;47m8[0;37;5;47;107m;.[0;1;30;90;47mS[0;1;30;90;44m888888[0;36;5;40;100mt...t[0m \n"
"  [0;36;5;40;100m:...t[0;1;30;90;44m8[0;37;5;40;100mX[0;37;5;47;107m;[0;1;30;90;47m:[0;1;30;90;44m@88[0;1;30;90;47mS[0;1;37;97;47m [0;1;30;90;44m@88888[0;36;5;40;100m@[0;37;5;47;107m  ;[0;36;5;40;100m [0;1;30;90;44m88[0;34;5;40;100m8[0;1;30;90;44m888[0;34;5;40;100m@[0;36;5;40;100m....[0m  \n"
"  [0;36;5;40;100m.[0;1;37;97;47m8[0;37;5;47;107m;t[0;37;5;40;100mS[0;36;5;40;100mS[0;1;30;90;44m8[0;34;5;40;100m8[0;1;30;90;47m@[0;1;37;97;47mX:[0;37;5;47;107m8[0;1;30;90;47m8[0;1;30;90;44m888888[0;36;5;40;100m [0;37;5;47;107m% @[0;36;5;40;100mX@[0;1;30;90;47m8S[0;36;5;40;100m@[0;1;30;90;44m88[0;34;5;40;100m@[0;36;5;40;100m:...[0;30;5;40;100mS[0m  \n"
"  [0;37;5;47;107mt[0;37;5;40;100m@[0;1;30;90;47m%[0;37;5;40;100m@[0;36;5;40;100m..;:[0;1;30;90;47m%;[0;36;5;40;100m;t[0;1;30;90;47m8XXX@S[0;1;37;97;47m%[0;37;5;47;107m. @[0;1;30;90;47mS@[0;36;5;40;100m [0;1;30;90;44m@888[0;34;5;40;100mS[0;36;5;40;100m....[0;30;5;40;100m%[0m   \n"
"   [0;1;30;90;47mSS%t;;[0;37;5;40;100m8[0;36;5;40;100m;[0;34;5;40;100m@[0;1;30;90;44m8888888[0;1;30;90;47mt[0;1;37;97;47mSS[0;1;30;90;47m%[0;1;30;90;44m888888[0;34;5;40;100mS[0;36;5;40;100m:....[0;30;5;40;100m8[0m    \n"
"      [0;30;5;40;100m@[0;36;5;40;100m:....:S[0;34;5;40;100mX8[0;1;30;90;44m888888888[0;34;5;40;100m@S[0;36;5;40;100mt.....[0;30;5;40;100m%[0m      \n"
"        [0;30;5;40;100m8[0;36;5;40;100m;.......:;tt;:.......:[0;30;5;40;100m@[0m        \n"
"           [0;1;30;90;40m8[0;30;5;40;100mS[0;36;5;40;100m:............:%[0;30;5;40;100m8[0m           \n"
"                  [0;1;30;90;40m8[0;30;5;40;100m88[0;1;30;90;40m8[0m                  \n";

#endif // ZCASH_METRICS_H
