// Copyright (c) 2010 Satoshi Nakamoto
// Copyright (c) 2009-2014 The Bitcoin Core developers
// Copyright (c) 2015-2020 The Zcash Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or https://www.opensource.org/licenses/mit-license.php .

#include "key_io.h"
#include "main.h"
#include "crypto/equihash.h"

#include "tinyformat.h"
#include "util.h"
#include "utilstrencodings.h"

#include <assert.h>
#include <optional>
#include <variant>

#include <boost/assign/list_of.hpp>

#include "chainparamsseeds.h"

static CBlock CreateGenesisBlock(const char* pszTimestamp, const CScript& genesisOutputScript, uint32_t nTime, const uint256& nNonce, const std::vector<unsigned char>& nSolution, uint32_t nBits, int32_t nVersion, const CAmount& genesisReward)
{
    // To create a genesis block for a new chain which is Overwintered:
    //   txNew.nVersion = OVERWINTER_TX_VERSION
    //   txNew.fOverwintered = true
    //   txNew.nVersionGroupId = OVERWINTER_VERSION_GROUP_ID
    //   txNew.nExpiryHeight = <default value>
    CMutableTransaction txNew;
    txNew.nVersion = 1;
    txNew.vin.resize(1);
    txNew.vout.resize(1);
    txNew.vin[0].scriptSig = CScript() << 520617983 << CScriptNum(4) << std::vector<unsigned char>((const unsigned char*)pszTimestamp, (const unsigned char*)pszTimestamp + strlen(pszTimestamp));
    txNew.vout[0].nValue = genesisReward;
    txNew.vout[0].scriptPubKey = genesisOutputScript;

    CBlock genesis;
    genesis.nTime    = nTime;
    genesis.nBits    = nBits;
    genesis.nNonce   = nNonce;
    genesis.nSolution = nSolution;
    genesis.nVersion = nVersion;
    genesis.vtx.push_back(txNew);
    genesis.hashPrevBlock.SetNull();
    genesis.hashMerkleRoot = genesis.BuildMerkleTree();
    return genesis;
}

/**
 * Build the genesis block. Note that the output of its generation
 * transaction cannot be spent since it did not originally exist in the
 * database (and is in any case of zero value).
 *
 * >>> from pyblake2 import blake2s
 * >>> 'Agape' + blake2s(b'For the whole law can be summed up in this one command: “Love your neighbor as yourself.” ZEC#1308152').hexdigest()
 *
 * CBlock(hash=00040fe8, ver=4, hashPrevBlock=00000000000000, hashMerkleRoot=c4eaa5, nTime=1477641360, nBits=1f07ffff, nNonce=4695, vtx=1)
 *   CTransaction(hash=c4eaa5, ver=1, vin.size=1, vout.size=1, nLockTime=0)
 *     CTxIn(COutPoint(000000, -1), coinbase 04ffff071f0104455a6361736830623963346565663862376363343137656535303031653335303039383462366665613335363833613763616331343161303433633432303634383335643334)
 *     CTxOut(nValue=0.00000000, scriptPubKey=0x5F1DF16B2B704C8A578D0B)
 *   vMerkleTree: c4eaa5
 */
static CBlock CreateGenesisBlock(uint32_t nTime, const uint256& nNonce, const std::vector<unsigned char>& nSolution, uint32_t nBits, int32_t nVersion, const CAmount& genesisReward)
{
    const char* pszTimestamp = "Agapeb73de0f79af165ae3713f2ea479a9036702e664cc247bbad63596a753e54c345";
    const CScript genesisOutputScript = CScript() << ParseHex("04678afdb0fe5548271967f1a67130b7105cd6a828e03909a67962e0ea1f61deb649f6bc3f4cef38c4f35504e51ec112de5c384df7ba0b8d578a4c702b6bf11d5f") << OP_CHECKSIG;
    return CreateGenesisBlock(pszTimestamp, genesisOutputScript, nTime, nNonce, nSolution, nBits, nVersion, genesisReward);
}

/**
 * Main network
 */
/**
 * What makes a good checkpoint block?
 * + Is surrounded by blocks with reasonable timestamps
 *   (no blocks before with a timestamp after, none after with
 *    timestamp before)
 * + Contains no strange transactions
 */

const arith_uint256 maxUint = UintToArith256(uint256S("ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"));

class CMainParams : public CChainParams {
public:
    CMainParams() {
        strNetworkID = "main";
        strCurrencyUnits = "AGP";
        bip44CoinType = 133; // As registered in https://github.com/satoshilabs/slips/blob/master/slip-0044.md
        consensus.fCoinbaseMustBeShielded = true;
        consensus.nSubsidySlowStartInterval = 20000;
        consensus.nPreBlossomSubsidyHalvingInterval = Consensus::PRE_BLOSSOM_HALVING_INTERVAL;
        consensus.nPostBlossomSubsidyHalvingInterval = POST_BLOSSOM_HALVING_INTERVAL(Consensus::PRE_BLOSSOM_HALVING_INTERVAL);
        consensus.nMajorityEnforceBlockUpgrade = 750;
        consensus.nMajorityRejectBlockOutdated = 950;
        consensus.nMajorityWindow = 4000;
        const size_t N = 200, K = 9;
        static_assert(equihash_parameters_acceptable(N, K));
        consensus.nEquihashN = N;
        consensus.nEquihashK = K;
        consensus.powLimit = uint256S("0007ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff");
        consensus.nPowAveragingWindow = 17;
        assert(maxUint/UintToArith256(consensus.powLimit) >= consensus.nPowAveragingWindow);
        consensus.nPowMaxAdjustDown = 32; // 32% adjustment down
        consensus.nPowMaxAdjustUp = 16; // 16% adjustment up
        consensus.nPreBlossomPowTargetSpacing = Consensus::PRE_BLOSSOM_POW_TARGET_SPACING;
        consensus.nPostBlossomPowTargetSpacing = Consensus::POST_BLOSSOM_POW_TARGET_SPACING;
        consensus.nPowAllowMinDifficultyBlocksAfterHeight = std::nullopt;
        consensus.fPowNoRetargeting = false;
        consensus.vUpgrades[Consensus::BASE_SPROUT].nProtocolVersion = 170002;
        consensus.vUpgrades[Consensus::BASE_SPROUT].nActivationHeight =
            Consensus::NetworkUpgrade::ALWAYS_ACTIVE;
        consensus.vUpgrades[Consensus::UPGRADE_TESTDUMMY].nProtocolVersion = 170002;
        consensus.vUpgrades[Consensus::UPGRADE_TESTDUMMY].nActivationHeight =
            Consensus::NetworkUpgrade::NO_ACTIVATION_HEIGHT;
        consensus.vUpgrades[Consensus::UPGRADE_OVERWINTER].nProtocolVersion = 170005;
        consensus.vUpgrades[Consensus::UPGRADE_OVERWINTER].nActivationHeight = 5;
        //consensus.vUpgrades[Consensus::UPGRADE_OVERWINTER].hashActivationBlock = uint256S("00079e5ec17821bffb15e94645703252dfa5fb6b6220412ecbe235cdc055e5ee");
        consensus.vUpgrades[Consensus::UPGRADE_SAPLING].nProtocolVersion = 170007;
        consensus.vUpgrades[Consensus::UPGRADE_SAPLING].nActivationHeight = 10;
        //consensus.vUpgrades[Consensus::UPGRADE_SAPLING].hashActivationBlock = uint256S("00058c9d7716c895808fca9b80d5d547559c2249e7be327746a43bc634c93955");
        consensus.vUpgrades[Consensus::UPGRADE_BLOSSOM].nProtocolVersion = 170009;
        consensus.vUpgrades[Consensus::UPGRADE_BLOSSOM].nActivationHeight = 15;
        //consensus.vUpgrades[Consensus::UPGRADE_BLOSSOM].hashActivationBlock = uint256S("00044117f6b0e2c8f92ea27d9816e6abd933ef1ae0dca5f6a2639ecbf7d85c87");
        consensus.vUpgrades[Consensus::UPGRADE_HEARTWOOD].nProtocolVersion = 170011;
        consensus.vUpgrades[Consensus::UPGRADE_HEARTWOOD].nActivationHeight = 20;
        //consensus.vUpgrades[Consensus::UPGRADE_HEARTWOOD].hashActivationBlock = uint256S("000738da064f69b1a7fedf4a466c01acd32642b5ed62b8439c9cacda9ee7a905");
        consensus.vUpgrades[Consensus::UPGRADE_CANOPY].nProtocolVersion = 170013;
        consensus.vUpgrades[Consensus::UPGRADE_CANOPY].nActivationHeight = 25;
        //consensus.vUpgrades[Consensus::UPGRADE_CANOPY].hashActivationBlock = uint256S("000312cedf197f858b12f19eea2fe919468f2c6d52d3d7c31848e3f23b15ea74");
        consensus.vUpgrades[Consensus::UPGRADE_NU5].nProtocolVersion = 170015;
        consensus.vUpgrades[Consensus::UPGRADE_NU5].nActivationHeight =
            Consensus::NetworkUpgrade::NO_ACTIVATION_HEIGHT;
        consensus.vUpgrades[Consensus::UPGRADE_ZFUTURE].nProtocolVersion = 0x7FFFFFFF;
        consensus.vUpgrades[Consensus::UPGRADE_ZFUTURE].nActivationHeight =
            Consensus::NetworkUpgrade::NO_ACTIVATION_HEIGHT;

        consensus.nFundingPeriodLength = consensus.nPostBlossomSubsidyHalvingInterval / 48;

        // guarantees the first 2 characters, when base58 encoded, are "a1"
        keyConstants.base58Prefixes[PUBKEY_ADDRESS]     = {0x12,0x96};
        // guarantees the first 2 characters, when base58 encoded, are "a3"
        keyConstants.base58Prefixes[SCRIPT_ADDRESS]     = {0x12,0x9B};
        // the first character, when base58 encoded, is "5" or "K" or "L" (as in Bitcoin)
        keyConstants.base58Prefixes[SECRET_KEY]         = {0x80};
        // do not rely on these BIP32 prefixes; they are not specified and may change
        keyConstants.base58Prefixes[EXT_PUBLIC_KEY]     = {0x04,0x88,0xB2,0x1E};
        keyConstants.base58Prefixes[EXT_SECRET_KEY]     = {0x04,0x88,0xAD,0xE4};
        // guarantees the first 2 characters, when base58 encoded, are "zc"
        keyConstants.base58Prefixes[ZCPAYMENT_ADDRESS]  = {0x16,0x9A};
        // guarantees the first 4 characters, when base58 encoded, are "ZiVK"
        keyConstants.base58Prefixes[ZCVIEWING_KEY]      = {0xA8,0xAB,0xD3};
        // guarantees the first 2 characters, when base58 encoded, are "SK"
        keyConstants.base58Prefixes[ZCSPENDING_KEY]     = {0xAB,0x36};

        keyConstants.bech32HRPs[SAPLING_PAYMENT_ADDRESS]      = "za";
        keyConstants.bech32HRPs[SAPLING_FULL_VIEWING_KEY]     = "zaviews";
        keyConstants.bech32HRPs[SAPLING_INCOMING_VIEWING_KEY] = "zaivks";
        keyConstants.bech32HRPs[SAPLING_EXTENDED_SPEND_KEY]   = "secret-extended-agape-key-main";
        keyConstants.bech32HRPs[SAPLING_EXTENDED_FVK]         = "zaxviews";

        {
            // Beneficial and Development funding each use a single address repeated 48 times,
            // once for each funding period.
            std::vector<std::string> beneficial_addresses(48, "a1bm5Ezz5LN9ropodgAjLMVPFzwiSW6VNAx");
            std::vector<std::string> development_addresses(48, "a1rsi3ukNTfAYtk66q3bUgp7svB3ynjSTv8");

            consensus.AddZIP207FundingStream(
                keyConstants,
                Consensus::FS_BENEFICIAL,
                consensus.vUpgrades[Consensus::UPGRADE_CANOPY].nActivationHeight, 100000,
                beneficial_addresses);
            consensus.AddZIP207FundingStream(
                keyConstants,
                Consensus::FS_DEVELOPMENT,
                consensus.vUpgrades[Consensus::UPGRADE_CANOPY].nActivationHeight, 100000,
                development_addresses);    
        }

        // The best chain should have at least this much work.
        consensus.nMinimumChainWork = uint256S("0000000000000000000000000000000000000000000000000000000000010000");

        /**
         * The message start string should be awesome! ⓩ❤
         */
        pchMessageStart[0] = 0x24;
        pchMessageStart[1] = 0xe9;
        pchMessageStart[2] = 0x27;
        pchMessageStart[3] = 0x64;
        vAlertPubKey = ParseHex("04ae82fda301351a4e0ddcbfbe4eabb377faef10a5ed8fb4792e9f09ff2f29f0f8b7ea0c5f8e7dcc5fe7eb649df8db165db141111f5736432946b8a35e1d2674f1");
        nDefaultPort = 8233;
        nPruneAfterHeight = 100000;

        genesis = CreateGenesisBlock(
            1625444828,
            uint256S("0x00000000000000000000000000000000000000000000000000000000000005f3"),
            ParseHex("00f8a92e924582bea78186269e5eba1eb7709e5d371931db6085cc406de4fed1f3cc2ac4d2542d35e19707e37f2e09127b43e8122a8fb1e68e42b12b1cfd640b1b06aade9ee83fe1e2c5a9b661ddd587cfb513b10b5bdb7706c581d9b7f582275e7fc1dd6f61158e2e0bf68f27c9deb5b9f67713886a44716dabe2f1fe8028f70406c80db539a991835b67be6c84f090521f53344fe7449d677b713e0fd6878746115e011ed0987e04d8a2bc7e1722d182d6112f43d2317466eaf6da222d6deb7ed84dffd7f9ba644dd02d44e5e13bd05f6305906c62fe5570cec166e6a71e75a3667fe3b4a8df342e842660592989eda8f43b2a4c5f99863bed08b60b4195e630af5a7192bd09a8be5f4c1f30197b586336f17ab3520f29c343f606ca587bbf8270b337ca0a13db453af4473147518ef22732751c14cd8e8eff5933389d836f92d68fd5b5a5dab36b42b2e0e19c8d5f0130bb5e67062e07da24a0188040d1488fa5eec96604f10491b36b2d17fd4d91777beeab0d10a3aadbd426726e3822a20035d985a30c32f6d6becf09fd619337f1eb0b402824715e18d3f202b21c71c51291b0da1a4b0af419315f8bb302cb960b7ffdb724dbbb8a6629b1477e4f9fd483f8c8629defb275de723359d71f1a6b09bda9cd861bea99f349fed785d57eb7ec8e6a23f2ee461eb89d19dcb66979a7f585769b5ed6dd1b0315f617bb0d0d7b070f918cfc2c8b57b178de0f7c1ed2079cbaf2aa27aa3674b6bbe19b56b672ba6fd914c4e686fcc9b9ae4e6e04d12ab2cfaadec15950fe1744eca7f311fdc1e6f24297fc9e97a24eb47398211397eb32ca0a198977a4f79c45cff02319bdf8f9353ec0b37d5cb27507ff45763ff47fec29e9d6f8472b203d5b7e16ee12fb9690c911adddd1027cecb559383ca3da1b131c831b03a3a4af7e5369239e7b9fec06019c9e78d61260cbd18bd33e63d69ae2393a5f93200637dabcf033d89bdedb6973fae14642833cf7a0fd2132cfe9146b0ed7c70a923f02b46ce8dd20fd8a7c3908fe6557e56fcbc2fa462cc1c28845eaa3fdeaf3038a5a4cfdc6f3f5519092a8443b52e8ba64e868b70e557e38139f254989fb73b6c6f9a0da1d0d3b80ab0e98eda50ac53a14ef8ad729f3fb7ac6af78b73826128c4902baca777f474d550220ee056e9678bca6cd06063d4a7a8ab147abbb94b054622df98b015d9c31484f04b887af0a4daa3cf5a802b451fad665bbe6b71b25049783cd600ce61bf4de32b0dc4d5cea5deb05386cfc2803292833b9c2490b8c650f7b52eb3e8ffb102dc4a3de658177e8a6f545192e7cadbb6172dbe0153b24bf630ba0cc6a54060b53bd7243891e7feeb03810bb381ef0fc7bae7b69ea9856c9f6ce2b57ed12498976d7231c7db194c1c5e522f6b17f5d83ff899f03d41a2ec9a1b227179a30651e9ed15ed3afbd2c8c0d365f7e25212b039869c195fcebc0fe560258ccac04c08edbb3af4043b24f226f3ac6105d3fcf398da90aa347c91aa9834d6019f1431bae531e9c59d8b12204ec5bdee9e1ebabc4ba97cc0e71f15211ef388262140937e4a097c145a1bae537adc64eaa20c49a7f5d21528f8dd415396d438de3436755b490ff48bd057131fe31da92fcb5b5ef7346f766e51cc296c6dd96be03eb383bcca95c196c2c63041fc1f998e85589b5965ec9c30e21603d0f22547ac949695f2749b73cd9a204c1fff7885c7e71eadb542d6bab297deda7d64dd047d1b581a9bc2f67eec0b4d4b6debc99ae149a6f8107093744b4097ec5052453f6c8b753b30025fba16b4ac25c8b6f19edc2db3c45561b5f8d65b86e3f936026c2bd5c4ba4bbb9c26b131662bded969cea1d7ddb5dbafb92b05a90c3e3c5b5ed36e7f72603dd3f6ce7"),
            0x1f07ffff, 4, 0);
        consensus.hashGenesisBlock = genesis.GetHash();
        assert(consensus.hashGenesisBlock == uint256S("0x0003bbd30b96c7d2f482937f0797ba86394ae817926b16cb33c04ae46881f518"));
        assert(genesis.hashMerkleRoot == uint256S("0xf9c23ee2d268e1cd26f86f40380dc865e41187d392382e72bf31215ef9e14fe7"));

        vFixedSeeds.clear();
        vSeeds.clear();
        vSeeds.push_back(CDNSSeedData("agape", "dnsseed.agape.money")); // Agape


        vFixedSeeds = std::vector<SeedSpec6>(pnSeed6_main, pnSeed6_main + ARRAYLEN(pnSeed6_main));

        fMiningRequiresPeers = true;
        fDefaultConsistencyChecks = false;
        fRequireStandard = true;
        fMineBlocksOnDemand = false;
        fTestnetToBeDeprecatedFieldRPC = false;

        checkpointData = (CCheckpointData) {
            boost::assign::map_list_of
            (0, consensus.hashGenesisBlock)
            (1510, uint256S("0x00045597225c55371c3c84b8be821b5b11e34c470a96611ef69c516426e1e41c")),
            1627786674,     // * UNIX timestamp of last checkpoint block
            1510,           // * total number of transactions between genesis and last checkpoint
            1               // * estimated number of transactions per day after checkpoint
                            //   total number of tx / (checkpoint block height / (24 * 24))
        };

        // Hardcoded fallback value for the Sprout shielded value pool balance
        // for nodes that have not reindexed since the introduction of monitoring
        // in #2795.
        nSproutValuePoolCheckpointHeight = 0;
        nSproutValuePoolCheckpointBalance = 0;
        fZIP209Enabled = true;
        hashSproutValuePoolCheckpointBlock = uint256S("0003bbd30b96c7d2f482937f0797ba86394ae817926b16cb33c04ae46881f518");
    }
};
static CMainParams mainParams;

/**
 * Testnet (v3)
 */
class CTestNetParams : public CChainParams {
public:
    CTestNetParams() {
        strNetworkID = "test";
        strCurrencyUnits = "TAZ";
        bip44CoinType = 1;
        consensus.fCoinbaseMustBeShielded = true;
        consensus.nSubsidySlowStartInterval = 20000;
        consensus.nPreBlossomSubsidyHalvingInterval = Consensus::PRE_BLOSSOM_HALVING_INTERVAL;
        consensus.nPostBlossomSubsidyHalvingInterval = POST_BLOSSOM_HALVING_INTERVAL(Consensus::PRE_BLOSSOM_HALVING_INTERVAL);
        consensus.nMajorityEnforceBlockUpgrade = 51;
        consensus.nMajorityRejectBlockOutdated = 75;
        consensus.nMajorityWindow = 400;
        const size_t N = 200, K = 9;
        static_assert(equihash_parameters_acceptable(N, K));
        consensus.nEquihashN = N;
        consensus.nEquihashK = K;
        consensus.powLimit = uint256S("07ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff");
        consensus.nPowAveragingWindow = 17;
        assert(maxUint/UintToArith256(consensus.powLimit) >= consensus.nPowAveragingWindow);
        consensus.nPowMaxAdjustDown = 32; // 32% adjustment down
        consensus.nPowMaxAdjustUp = 16; // 16% adjustment up
        consensus.nPreBlossomPowTargetSpacing = Consensus::PRE_BLOSSOM_POW_TARGET_SPACING;
        consensus.nPostBlossomPowTargetSpacing = Consensus::POST_BLOSSOM_POW_TARGET_SPACING;
        consensus.nPowAllowMinDifficultyBlocksAfterHeight = 299187;
        consensus.fPowNoRetargeting = false;
        consensus.vUpgrades[Consensus::BASE_SPROUT].nProtocolVersion = 170002;
        consensus.vUpgrades[Consensus::BASE_SPROUT].nActivationHeight =
            Consensus::NetworkUpgrade::ALWAYS_ACTIVE;
        consensus.vUpgrades[Consensus::UPGRADE_TESTDUMMY].nProtocolVersion = 170002;
        consensus.vUpgrades[Consensus::UPGRADE_TESTDUMMY].nActivationHeight =
            Consensus::NetworkUpgrade::NO_ACTIVATION_HEIGHT;
        consensus.vUpgrades[Consensus::UPGRADE_OVERWINTER].nProtocolVersion = 170003;
        consensus.vUpgrades[Consensus::UPGRADE_OVERWINTER].nActivationHeight = 2;
        //consensus.vUpgrades[Consensus::UPGRADE_OVERWINTER].hashActivationBlock =
        //    uint256S("0000257c4331b098045023fcfbfa2474681f4564ab483f84e4e1ad078e4acf44");
        consensus.vUpgrades[Consensus::UPGRADE_SAPLING].nProtocolVersion = 170007;
        consensus.vUpgrades[Consensus::UPGRADE_SAPLING].nActivationHeight = 4;
        //consensus.vUpgrades[Consensus::UPGRADE_SAPLING].hashActivationBlock =
        //    uint256S("000420e7fcc3a49d729479fb0b560dd7b8617b178a08e9e389620a9d1dd6361a");
        consensus.vUpgrades[Consensus::UPGRADE_BLOSSOM].nProtocolVersion = 170008;
        consensus.vUpgrades[Consensus::UPGRADE_BLOSSOM].nActivationHeight = 6;
        //consensus.vUpgrades[Consensus::UPGRADE_BLOSSOM].hashActivationBlock =
        //    uint256S("00367515ef2e781b8c9358b443b6329572599edd02c59e8af67db9785122f298");
        consensus.vUpgrades[Consensus::UPGRADE_HEARTWOOD].nProtocolVersion = 170010;
        consensus.vUpgrades[Consensus::UPGRADE_HEARTWOOD].nActivationHeight = 8;
        //consensus.vUpgrades[Consensus::UPGRADE_HEARTWOOD].hashActivationBlock =
        //    uint256S("05688d8a0e9ff7c04f6f05e6d695dc5ab43b9c4803342d77ae360b2b27d2468e");
        consensus.vUpgrades[Consensus::UPGRADE_CANOPY].nProtocolVersion = 170012;
        consensus.vUpgrades[Consensus::UPGRADE_CANOPY].nActivationHeight = 10;
        //consensus.vUpgrades[Consensus::UPGRADE_CANOPY].hashActivationBlock =
        //    uint256S("01a4d7c6aada30c87762c1bf33fff5df7266b1fd7616bfdb5227fa59bd79e7a2");
        consensus.vUpgrades[Consensus::UPGRADE_NU5].nProtocolVersion = 170014;
        consensus.vUpgrades[Consensus::UPGRADE_NU5].nActivationHeight =
            Consensus::NetworkUpgrade::NO_ACTIVATION_HEIGHT;
        consensus.vUpgrades[Consensus::UPGRADE_ZFUTURE].nProtocolVersion = 0x7FFFFFFF;
        consensus.vUpgrades[Consensus::UPGRADE_ZFUTURE].nActivationHeight =
            Consensus::NetworkUpgrade::NO_ACTIVATION_HEIGHT;

        consensus.nFundingPeriodLength = consensus.nPostBlossomSubsidyHalvingInterval / 48;

        // guarantees the first 2 characters, when base58 encoded, are "am"
        keyConstants.base58Prefixes[PUBKEY_ADDRESS]     = {0x13,0x03};
        // guarantees the first 2 characters, when base58 encoded, are "a2"
        keyConstants.base58Prefixes[SCRIPT_ADDRESS]     = {0x12,0x98};
        // the first character, when base58 encoded, is "9" or "c" (as in Bitcoin)
        keyConstants.base58Prefixes[SECRET_KEY]         = {0xEF};
        // do not rely on these BIP32 prefixes; they are not specified and may change
        keyConstants.base58Prefixes[EXT_PUBLIC_KEY]     = {0x04,0x35,0x87,0xCF};
        keyConstants.base58Prefixes[EXT_SECRET_KEY]     = {0x04,0x35,0x83,0x94};
        // guarantees the first 2 characters, when base58 encoded, are "zt"
        keyConstants.base58Prefixes[ZCPAYMENT_ADDRESS]  = {0x16,0xB6};
        // guarantees the first 4 characters, when base58 encoded, are "ZiVt"
        keyConstants.base58Prefixes[ZCVIEWING_KEY]      = {0xA8,0xAC,0x0C};
        // guarantees the first 2 characters, when base58 encoded, are "ST"
        keyConstants.base58Prefixes[ZCSPENDING_KEY]     = {0xAC,0x08};

        keyConstants.bech32HRPs[SAPLING_PAYMENT_ADDRESS]      = "ztestagape";
        keyConstants.bech32HRPs[SAPLING_FULL_VIEWING_KEY]     = "zviewtestagape";
        keyConstants.bech32HRPs[SAPLING_INCOMING_VIEWING_KEY] = "zivktestagape";
        keyConstants.bech32HRPs[SAPLING_EXTENDED_SPEND_KEY]   = "secret-extended-agape-key-test";
        keyConstants.bech32HRPs[SAPLING_EXTENDED_FVK]         = "zxviewtestagape";

        // Testnet funding streams
        {
            // Beneficial and Development funding each use a single address repeated 51 times,
            // once for each funding period.
            std::vector<std::string> beneficial_addresses(51, "amUtwo5equEC16qW25EUynSMDhQzf7AJhSe");
            std::vector<std::string> development_addresses(51, "amYA2Tn8yrNjtAbKa8TX7mHxBxhB2eHb4Qr");

            consensus.AddZIP207FundingStream(
                keyConstants,
                Consensus::FS_BENEFICIAL,
                consensus.vUpgrades[Consensus::UPGRADE_CANOPY].nActivationHeight, 100000,
                beneficial_addresses);
            consensus.AddZIP207FundingStream(
                keyConstants,
                Consensus::FS_DEVELOPMENT,
                consensus.vUpgrades[Consensus::UPGRADE_CANOPY].nActivationHeight, 100000,
                development_addresses);    
        }

        // On testnet we activate this rule 6 blocks after Blossom activation. From block 299188 and
        // prior to Blossom activation, the testnet minimum-difficulty threshold was 15 minutes (i.e.
        // a minimum difficulty block can be mined if no block is mined normally within 15 minutes):
        // <https://zips.z.cash/zip-0205#change-to-difficulty-adjustment-on-testnet>
        // However the median-time-past is 6 blocks behind, and the worst-case time for 7 blocks at a
        // 15-minute spacing is ~105 minutes, which exceeds the limit imposed by the soft fork of
        // 90 minutes.
        //
        // After Blossom, the minimum difficulty threshold time is changed to 6 times the block target
        // spacing, which is 7.5 minutes:
        // <https://zips.z.cash/zip-0208#minimum-difficulty-blocks-on-the-test-network>
        // 7 times that is 52.5 minutes which is well within the limit imposed by the soft fork.

        static_assert(6 * Consensus::POST_BLOSSOM_POW_TARGET_SPACING * 7 < MAX_FUTURE_BLOCK_TIME_MTP - 60,
                      "MAX_FUTURE_BLOCK_TIME_MTP is too low given block target spacing");
        consensus.nFutureTimestampSoftForkHeight = consensus.vUpgrades[Consensus::UPGRADE_BLOSSOM].nActivationHeight + 6;

        // The best chain should have at least this much work.
        consensus.nMinimumChainWork = uint256S("0000000000000000000000000000000000000000000000000000000000000001");

        pchMessageStart[0] = 0xfa;
        pchMessageStart[1] = 0x1a;
        pchMessageStart[2] = 0xf9;
        pchMessageStart[3] = 0xbf;
        vAlertPubKey = ParseHex("04ae82fda301351a4e0ddcbfbe4eabb377faef10a5ed8fb4792e9f09ff2f29f0f8b7ea0c5f8e7dcc5fe7eb649df8db165db141111f5736432946b8a35e1d2674f1");
        nDefaultPort = 18233;
        nPruneAfterHeight = 1000;

        genesis = CreateGenesisBlock(
            1627956901,
            uint256S("0x0000000000000000000000000000000000000000000000000000000000000008"),
            ParseHex("00acc5a579caa320a6a164569bec81ed9a2af0ec090493becec6d0cf49b8567238aa95633f6c939bba5215f1d679eb86cb497fa9f2aabea59b09b6bfd368b8572a05445b5d01c5d199572297e2a903c45f1ffda40b67ff2adaccb6136070c8b0ff51e3b6397539d13518d91b9b8a46d1fc8493330b7d43ed4e4302dc239210400534f1d462353dc2d56b1cad2a4b228a7c893b47d157dfee19fbbcf129f4fb404168eb8a4bdc60ab02ee3bab701237299fdda30f47214838d4bf6c6f1828a71a69a5ddc8e30cbf1570cdc2e6fd812076e5df2ad52a4ff85896edaed4d7f68ae81f0260f9fea14d2bfa134f7f5b6e4519562355afdc660698e5371d450619622f335e937307b352727c2e9ebd09fb6d9fa4062190427482e4ef999881fc9cf492a30d7e9e85af16283f4619cbf285a49fd55f80d7e7ed8d7d3822851a98eb99f68ea94d283c344607508a7b89781f6227012e32ddc30727969bb30082a4e644cc9bf7472f0008d4bce78d0673588031d4af795f5726a0db16867e0a006cda0210781b69129127a328c85d84bc5f691e0a7891a6960fa4c718fb14483ec9db15869035b6a503557aa944154a99f804e43b08658f6595369248692fd1353b3c4ceaf5ac7dc4bceadbf749d2c773b70b0ffb8a048b1921ebce47e18dfa72a510b7699f641d1a45dde0af47cf0793ca77fd23c5a87e395f5a21cc04be2fc8ec87d28510ce24431b4a0b3dc030f893772c5685d5dfdd51afec00b32f1b7b95d989fbae3f6b05833da0315f89d95882d05eccbf794ab7e918ed1e37a2570cff6d4603a63a2adb746ec00716b97911940c4801e312471f9509dcb4aa1fcc5a3b0ac7bce504432b12adf2d58c6d9f40a44a37ef4d4a59b25b41380ee049cd5a65fd054fd2a1d0335eb6725fbab935c03a46e23fb5946e1f7ccf08faa960b36270a6fe50fc0155de2a7449614deaf3f268fc24a0f8d6756ff2d50307467d27cc5c1307976ab4c6761b270fd99e118a1390e2d6570b172ca6163378791d08b5131b7fc32b4830cc070665c1173bbe35f0925159c5cdd5d3de44047d6d96e411441effa131adc165f8a1f76e53d601141eb205bece03c6c441930b17dffb14f8282c4e5829ba5ec5f14d5a1a714138d8fe6a878f772ffe191c5b60237e4cf1320db462c741c4be6b99eca91ce89e04d3bfc778c52ce7d07d33dd1c5e78de829cbb98533a7a3fdbcbe6c687d3618524a3bc1def32ab9d73b7209afa56a7152193968a340f02f498e13eab0c83b23c0849e84bd8adc97cc6463d5175d54378209f61810681909753514f05fdb307d759f476261374328e0a0eb648ae6418e13b48b3c1d8e43430ed8ff8b03c561a2fb6a1e50d4cbd1d9c95ace54addfed402d8c87b25cb0423948b15adf8bc45743a6a6393287d9d79fb01d675305212adc6b18a952038f3c1fa09c5b7880f6451eb9d89da78dfe983b887a0548ffb78ecff5e790f353b27b26e56eddede441c5941c927029efdef5015b13cb13de7e03f7ab01878cfe055e703465daadf11ba4b384b4d8cebeacb38c4abe2ba4fb7c5de0dd145e45cb14718b8d5ba505a592f67416ebfbf9b733120524aeaad629cfb504823e6b55978f906a7973a7539fe465451189887f7d3c3c71d7398e578e16f98a80213911e09cade42de84a4024abd37b54a53f5526e43f85d803592e89d4087e52ed4b9175a1b3f52bf5e09b8695486f46dadceba81f77c22dd82ef05d9de030d3182191f2511ed9baab266a47d4b8581a0388d3710124cc448263b61ad03817de6ef15927d11db5fdb17b102b7d211dd855ba9d4ec7e2a03cd6d1a1efe3c1f9d6b89aed481dfd98b06772bb661ebc8fedfbc0e27b5b1c65bd27bfd7eb8162648d51f3ec6061c47da"),
            0x2007ffff, 4, 0);
        consensus.hashGenesisBlock = genesis.GetHash();
        assert(consensus.hashGenesisBlock == uint256S("0x019c8e34e71207ed95988c8120c3ebc5b071c572b8bdc17b75a9e5449e04f4e2"));
        assert(genesis.hashMerkleRoot == uint256S("0xf9c23ee2d268e1cd26f86f40380dc865e41187d392382e72bf31215ef9e14fe7"));

        vFixedSeeds.clear();
        vSeeds.clear();
        vSeeds.push_back(CDNSSeedData("Agape", "dnsseed.testnet.agape.money")); // Agape

        vFixedSeeds = std::vector<SeedSpec6>(pnSeed6_test, pnSeed6_test + ARRAYLEN(pnSeed6_test));

        fMiningRequiresPeers = false;
        fDefaultConsistencyChecks = false;
        fRequireStandard = true;
        fMineBlocksOnDemand = false;
        fTestnetToBeDeprecatedFieldRPC = true;


        checkpointData = (CCheckpointData) {
            boost::assign::map_list_of
            (0, consensus.hashGenesisBlock),
            1627953753,  // * UNIX timestamp of last checkpoint block
            0            // * total number of transactions between genesis and last checkpoint
                         //   total number of tx / (checkpoint block height / (24 * 24))
        };

        // Hardcoded fallback value for the Sprout shielded value pool balance
        // for nodes that have not reindexed since the introduction of monitoring
        // in #2795.
        nSproutValuePoolCheckpointHeight = 0;
        nSproutValuePoolCheckpointBalance = 0;
        fZIP209Enabled = true;
        hashSproutValuePoolCheckpointBlock = uint256S("0755a5df61b1e81f933c553e8175dd23f8bc26746cf3e8bc3de3a03579de0eee");
    }
};
static CTestNetParams testNetParams;

/**
 * Regression test
 */
class CRegTestParams : public CChainParams {
public:
    CRegTestParams() {
        strNetworkID = "regtest";
        strCurrencyUnits = "REG";
        bip44CoinType = 1;
        consensus.fCoinbaseMustBeShielded = false;
        consensus.nSubsidySlowStartInterval = 0;
        consensus.nPreBlossomSubsidyHalvingInterval = Consensus::PRE_BLOSSOM_REGTEST_HALVING_INTERVAL;
        consensus.nPostBlossomSubsidyHalvingInterval = POST_BLOSSOM_HALVING_INTERVAL(Consensus::PRE_BLOSSOM_REGTEST_HALVING_INTERVAL);
        consensus.nMajorityEnforceBlockUpgrade = 750;
        consensus.nMajorityRejectBlockOutdated = 950;
        consensus.nMajorityWindow = 1000;
        const size_t N = 48, K = 5;
        static_assert(equihash_parameters_acceptable(N, K));
        consensus.nEquihashN = N;
        consensus.nEquihashK = K;
        consensus.powLimit = uint256S("0f0f0f0f0f0f0f0f0f0f0f0f0f0f0f0f0f0f0f0f0f0f0f0f0f0f0f0f0f0f0f0f");
        consensus.nPowAveragingWindow = 17;
        assert(maxUint/UintToArith256(consensus.powLimit) >= consensus.nPowAveragingWindow);
        consensus.nPowMaxAdjustDown = 0; // Turn off adjustment down
        consensus.nPowMaxAdjustUp = 0; // Turn off adjustment up
        consensus.nPreBlossomPowTargetSpacing = Consensus::PRE_BLOSSOM_POW_TARGET_SPACING;
        consensus.nPostBlossomPowTargetSpacing = Consensus::POST_BLOSSOM_POW_TARGET_SPACING;
        consensus.nPowAllowMinDifficultyBlocksAfterHeight = 0;
        consensus.fPowNoRetargeting = true;
        consensus.vUpgrades[Consensus::BASE_SPROUT].nProtocolVersion = 170002;
        consensus.vUpgrades[Consensus::BASE_SPROUT].nActivationHeight =
            Consensus::NetworkUpgrade::ALWAYS_ACTIVE;
        consensus.vUpgrades[Consensus::UPGRADE_TESTDUMMY].nProtocolVersion = 170002;
        consensus.vUpgrades[Consensus::UPGRADE_TESTDUMMY].nActivationHeight =
            Consensus::NetworkUpgrade::NO_ACTIVATION_HEIGHT;
        consensus.vUpgrades[Consensus::UPGRADE_OVERWINTER].nProtocolVersion = 170003;
        consensus.vUpgrades[Consensus::UPGRADE_OVERWINTER].nActivationHeight =
            Consensus::NetworkUpgrade::NO_ACTIVATION_HEIGHT;
        consensus.vUpgrades[Consensus::UPGRADE_SAPLING].nProtocolVersion = 170006;
        consensus.vUpgrades[Consensus::UPGRADE_SAPLING].nActivationHeight =
            Consensus::NetworkUpgrade::NO_ACTIVATION_HEIGHT;
        consensus.vUpgrades[Consensus::UPGRADE_BLOSSOM].nProtocolVersion = 170008;
        consensus.vUpgrades[Consensus::UPGRADE_BLOSSOM].nActivationHeight =
            Consensus::NetworkUpgrade::NO_ACTIVATION_HEIGHT;
        consensus.vUpgrades[Consensus::UPGRADE_HEARTWOOD].nProtocolVersion = 170010;
        consensus.vUpgrades[Consensus::UPGRADE_HEARTWOOD].nActivationHeight =
            Consensus::NetworkUpgrade::NO_ACTIVATION_HEIGHT;
        consensus.vUpgrades[Consensus::UPGRADE_CANOPY].nProtocolVersion = 170012;
        consensus.vUpgrades[Consensus::UPGRADE_CANOPY].nActivationHeight =
            Consensus::NetworkUpgrade::NO_ACTIVATION_HEIGHT;
        consensus.vUpgrades[Consensus::UPGRADE_NU5].nProtocolVersion = 170014;
        consensus.vUpgrades[Consensus::UPGRADE_NU5].nActivationHeight =
            Consensus::NetworkUpgrade::NO_ACTIVATION_HEIGHT;
        consensus.vUpgrades[Consensus::UPGRADE_ZFUTURE].nProtocolVersion = 0x7FFFFFFF;
        consensus.vUpgrades[Consensus::UPGRADE_ZFUTURE].nActivationHeight =
            Consensus::NetworkUpgrade::NO_ACTIVATION_HEIGHT;

        consensus.nFundingPeriodLength = consensus.nPostBlossomSubsidyHalvingInterval / 48;
        // Defined funding streams can be enabled with node config flags.

        // These prefixes are the same as the testnet prefixes
        keyConstants.base58Prefixes[PUBKEY_ADDRESS]     = {0x1D,0x25};
        keyConstants.base58Prefixes[SCRIPT_ADDRESS]     = {0x1C,0xBA};
        keyConstants.base58Prefixes[SECRET_KEY]         = {0xEF};
        // do not rely on these BIP32 prefixes; they are not specified and may change
        keyConstants.base58Prefixes[EXT_PUBLIC_KEY]     = {0x04,0x35,0x87,0xCF};
        keyConstants.base58Prefixes[EXT_SECRET_KEY]     = {0x04,0x35,0x83,0x94};
        keyConstants.base58Prefixes[ZCPAYMENT_ADDRESS]  = {0x16,0xB6};
        keyConstants.base58Prefixes[ZCVIEWING_KEY]      = {0xA8,0xAC,0x0C};
        keyConstants.base58Prefixes[ZCSPENDING_KEY]     = {0xAC,0x08};

        keyConstants.bech32HRPs[SAPLING_PAYMENT_ADDRESS]      = "zregtestsapling";
        keyConstants.bech32HRPs[SAPLING_FULL_VIEWING_KEY]     = "zviewregtestsapling";
        keyConstants.bech32HRPs[SAPLING_INCOMING_VIEWING_KEY] = "zivkregtestsapling";
        keyConstants.bech32HRPs[SAPLING_EXTENDED_SPEND_KEY]   = "secret-extended-key-regtest";
        keyConstants.bech32HRPs[SAPLING_EXTENDED_FVK]         = "zxviewregtestsapling";

        // The best chain should have at least this much work.
        consensus.nMinimumChainWork = uint256S("0x00");

        pchMessageStart[0] = 0xaa;
        pchMessageStart[1] = 0xe8;
        pchMessageStart[2] = 0x3f;
        pchMessageStart[3] = 0x5f;
        nDefaultPort = 18344;
        nPruneAfterHeight = 1000;

        genesis = CreateGenesisBlock(
            1296688602,
            uint256S("0x0000000000000000000000000000000000000000000000000000000000000009"),
            ParseHex("01936b7db1eb4ac39f151b8704642d0a8bda13ec547d54cd5e43ba142fc6d8877cab07b3"),
            0x200f0f0f, 4, 0);
        consensus.hashGenesisBlock = genesis.GetHash();
        //assert(consensus.hashGenesisBlock == uint256S("0x029f11d80ef9765602235e1bc9727e3eb6ba20839319f761fee920d63401e327"));
        //assert(genesis.hashMerkleRoot == uint256S("0xc4eaa58879081de3c24a7b117ed2b28300e7ec4c4c1dff1d3f1268b7857a4ddb"));

        vFixedSeeds.clear(); //!< Regtest mode doesn't have any fixed seeds.
        vSeeds.clear();      //!< Regtest mode doesn't have any DNS seeds.

        fMiningRequiresPeers = false;
        fDefaultConsistencyChecks = true;
        fRequireStandard = false;
        fMineBlocksOnDemand = true;
        fTestnetToBeDeprecatedFieldRPC = false;

        checkpointData = (CCheckpointData){
            boost::assign::map_list_of
            ( 0, uint256S("0f9188f13cb7b2c71f2a335e3a4fc328bf5beb436012afca590b1a11466e2206")),
            0,
            0,
            0
        };
    }

    void UpdateNetworkUpgradeParameters(Consensus::UpgradeIndex idx, int nActivationHeight)
    {
        assert(idx > Consensus::BASE_SPROUT && idx < Consensus::MAX_NETWORK_UPGRADES);
        consensus.vUpgrades[idx].nActivationHeight = nActivationHeight;
    }

    void UpdateFundingStreamParameters(Consensus::FundingStreamIndex idx, Consensus::FundingStream fs)
    {
        assert(idx >= Consensus::FIRST_FUNDING_STREAM && idx < Consensus::MAX_FUNDING_STREAMS);
        consensus.vFundingStreams[idx] = fs;
    }

    void UpdateRegtestPow(
        int64_t nPowMaxAdjustDown,
        int64_t nPowMaxAdjustUp,
        uint256 powLimit,
        bool noRetargeting)
    {
        consensus.nPowMaxAdjustDown = nPowMaxAdjustDown;
        consensus.nPowMaxAdjustUp = nPowMaxAdjustUp;
        consensus.powLimit = powLimit;
        consensus.fPowNoRetargeting = noRetargeting;
    }

    void SetRegTestZIP209Enabled() {
        fZIP209Enabled = true;
    }
};
static CRegTestParams regTestParams;

static CChainParams *pCurrentParams = 0;

const CChainParams &Params() {
    assert(pCurrentParams);
    return *pCurrentParams;
}

CChainParams& Params(const std::string& chain)
{
    if (chain == CBaseChainParams::MAIN)
            return mainParams;
    else if (chain == CBaseChainParams::TESTNET)
            return testNetParams;
    else if (chain == CBaseChainParams::REGTEST)
            return regTestParams;
    else
        throw std::runtime_error(strprintf("%s: Unknown chain %s.", __func__, chain));
}

void SelectParams(const std::string& network)
{
    SelectBaseParams(network);
    pCurrentParams = &Params(network);

    // Some python qa rpc tests need to enforce the coinbase consensus rule
    if (network == CBaseChainParams::REGTEST && mapArgs.count("-regtestshieldcoinbase")) {
        regTestParams.SetRegTestCoinbaseMustBeShielded();
    }

    // When a developer is debugging turnstile violations in regtest mode, enable ZIP209
    if (network == CBaseChainParams::REGTEST && mapArgs.count("-developersetpoolsizezero")) {
        regTestParams.SetRegTestZIP209Enabled();
    }
}

void UpdateNetworkUpgradeParameters(Consensus::UpgradeIndex idx, int nActivationHeight)
{
    regTestParams.UpdateNetworkUpgradeParameters(idx, nActivationHeight);
}

void UpdateFundingStreamParameters(Consensus::FundingStreamIndex idx, Consensus::FundingStream fs)
{
    regTestParams.UpdateFundingStreamParameters(idx, fs);
}

void UpdateRegtestPow(
    int64_t nPowMaxAdjustDown,
    int64_t nPowMaxAdjustUp,
    uint256 powLimit,
    bool noRetargeting)
{
    regTestParams.UpdateRegtestPow(nPowMaxAdjustDown, nPowMaxAdjustUp, powLimit, noRetargeting);
}
